package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VersionCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-v";
    }

    @Override
    @Nullable
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("\n***Version***");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

}

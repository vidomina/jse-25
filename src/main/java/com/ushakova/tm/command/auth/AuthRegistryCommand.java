package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthRegistryCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "New user registration.";
    }

    @Override
    public void execute() {
        System.out.println("Registration new user:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter Email:");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

    @Override
    @NotNull
    public String name() {
        return "registration";
    }

}

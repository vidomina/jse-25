# TASK MANAGER

Console application for task list.

## DEVELOPER INFO

NAME:   Valentina Ushakova  
E-MAIL: vushakova@t1-consulting.com

## SOFTWARE

* JDK 15.0.1
* Windows 10

## HARDWARE

* RAM 16Gb
* CPU i5
* HDD 128Gb

## RUN PROGRAM

    java -jar ./task-manager.jar

## BUILD PROGRAM

    mvn clean install

## FUTURE SCOPE

Use object-oriented programming best practices.

## SCREENSHOTS

[Screenshot URL.](https://drive.google.com/drive/folders/1Faa4ENLqRJtCWF_eLTUku9hhZeF5k94O?usp=sharing)

